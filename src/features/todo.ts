import { createSlice } from "@reduxjs/toolkit";

const initialState = {
	todos: [
		{ name: "Complete online JavaScript Course", id: 1, isCompleted: true },
		{ name: "Jog around the park 3x", id: 2, isCompleted: false },
		{ name: "10 minutes meditation", id: 3, isCompleted: true },
		{ name: "Read for 1 hour", id: 4, isCompleted: false },
		{ name: "Pickup groceries", id: 5, isCompleted: false },
	],
};

export const todo = createSlice({
	name: "todo",
	initialState,
	reducers: {
		addNewTask: (state, action) => {
			const newTask = {
				id: new Date().getTime(),
				isCompleted: false,
				name: action.payload.name,
			};
			state.todos.push(newTask);
		},
		toggleComplete: (state, action) => {
			state.todos.map((t) => {
				if (t.id === action.payload) t.isCompleted = !t.isCompleted;
				return t;
			});
		},
		deleteTask: (state, action) => {
			state.todos = state.todos.filter((t: any) => t.id != action.payload);
		},
		deleteCompletedTasks: (state) => {
			state.todos = state.todos.filter((t: any) => !t.isCompleted);
		},
	},
});
export const { addNewTask, toggleComplete, deleteTask, deleteCompletedTasks } = todo.actions;
export default todo.reducer;
