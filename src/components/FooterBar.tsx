import React from "react";
import { useSelector } from "react-redux";

interface FooterBarProp {
	allTodo: () => void;
	allActive: () => void;
	allCompleted: () => void;
	deleteAllCompleted: () => void;
}

export default function FooterBar({ allTodo, allActive, allCompleted, deleteAllCompleted, ...props }: FooterBarProp) {
	const todoData = useSelector((state: any) => state.todo);
	return (
		<div className="flex flex-row justify-between px-4 text-slate-700 py-3">
			<span>{todoData.todos.filter((t: any) => t.isCompleted == false).length} items left</span>
			<div className="grow flex items-center justify-center space-x-4">
				<button onClick={allTodo}>All</button>
				<button onClick={allActive}>Active</button>
				<button onClick={allCompleted}>Completed</button>
			</div>
			<button onClick={deleteAllCompleted}>Clear Completed</button>
		</div>
	);
}
