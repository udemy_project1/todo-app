import React, { useState } from "react";
import radio from "../assets/radio-70.svg";
import { useDispatch } from "react-redux";
import { addNewTask } from "../features/todo";

export default function InputTodo() {
	const [newTask, setNewTask] = useState("");
	const dispatch = useDispatch();
	const handleAddTask = (e: any) => {
		e.preventDefault();
		dispatch(addNewTask({ name: newTask }));
		setNewTask("");
	};
	return (
		<div className="flex bg-white items-center justify-center px-2 rounded-lg py-2">
			<span className="text-gray-400 text-4xl">
				<svg
					className="svg-icon"
					style={{ width: "1em", height: "1em", verticalAlign: "middle", fill: "currentColor", overflow: "hidden" }}
					viewBox="0 0 1024 1024"
					version="1.1"
					xmlns="http://www.w3.org/2000/svg">
					<path d="M512 810.667c-164.693 0-298.667-134.016-298.667-298.667s133.973-298.667 298.667-298.667c164.651 0 298.667 134.016 298.667 298.667s-134.016 298.667-298.667 298.667zM512 253.184c-142.72 0-258.859 116.096-258.859 258.816s116.139 258.816 258.859 258.816c142.763 0 258.816-116.181 258.816-258.816 0-142.763-116.053-258.816-258.816-258.816z" />
				</svg>
			</span>
			<form
				className="flex grow"
				onSubmit={(e) => handleAddTask(e)}>
				<input
					type="text"
					className="flex grow w-full focus:outline-0 text-gray-500 py-2 px-2 text-lg"
					placeholder="Type a new task"
					onChange={(e) => setNewTask(e.target.value)}
					value={newTask}
				/>
			</form>
		</div>
	);
}
