import React, { useEffect, useState } from "react";
import TaskItem from "./TaskItem";
import FooterBar from "./FooterBar";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { deleteCompletedTasks } from "../features/todo";

export default function TaskList() {
	const dispatch = useDispatch();
	const todoData = useSelector((state: any) => state.todo);
	const [tasks, setTasks] = useState([]);

	useEffect(() => {
		setTasks(todoData.todos);
	}, [todoData]);

	const getAllTodo = () => {
		setTasks(todoData.todos);
	};
	const getAllActive = () => {
		const activeTasks = todoData.todos.filter((t: any) => !t.isCompleted);
		setTasks(activeTasks);
	};
	const getAllCompleted = () => {
		const completedTasks = todoData.todos.filter((t: any) => t.isCompleted);
		setTasks(completedTasks);
	};
	const handleDeleteCompletedTasks = () => {
		dispatch(deleteCompletedTasks());
	};
	return (
		<div className="flex flex-col rounded-md px-1 border border-slate-500 bg-white">
			{tasks &&
				tasks.map((task: any) => (
					<TaskItem
						task={task}
						key={task.id}
					/>
				))}
			<FooterBar
				allTodo={getAllTodo}
				allActive={getAllActive}
				allCompleted={getAllCompleted}
				deleteAllCompleted={handleDeleteCompletedTasks}
			/>
		</div>
	);
}
