import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { toggleComplete, deleteTask } from "../features/todo";

export default function TaskItem({ task }: any) {
	const dispatch = useDispatch();
	const [hover, setHover] = useState(false);
	return (
		<div
			className="flex flex-row items-center bg-white px-0 py-4 mr-2 border-b border-b-slate-300 cursor-pointer"
			onMouseOver={() => setHover(true)}
			onMouseLeave={() => setHover(false)}>
			{!task.isCompleted ? (
				<button
					onClick={() => dispatch(toggleComplete(task.id))}
					className="text-gray-400 text-[35px] w-[40px] flex items-center justify-center">
					<svg
						className="svg-icon"
						style={{ width: "1em", height: "1em", verticalAlign: "middle", fill: "currentColor", overflow: "hidden" }}
						viewBox="0 0 1024 1024"
						version="1.1"
						xmlns="http://www.w3.org/2000/svg">
						<path d="M512 810.667c-164.693 0-298.667-134.016-298.667-298.667s133.973-298.667 298.667-298.667c164.651 0 298.667 134.016 298.667 298.667s-134.016 298.667-298.667 298.667zM512 253.184c-142.72 0-258.859 116.096-258.859 258.816s116.139 258.816 258.859 258.816c142.763 0 258.816-116.181 258.816-258.816 0-142.763-116.053-258.816-258.816-258.816z" />
					</svg>
				</button>
			) : (
				<button
					onClick={() => dispatch(toggleComplete(task.id))}
					className="text-gray-400 text-[25px] w-[40px] flex items-center justify-center">
					<svg
						className="svg-icon"
						style={{ width: "1em", height: "1em", verticalAlign: "middle", fill: "currentColor", overflow: "hidden" }}
						viewBox="0 0 1024 1024"
						version="1.1"
						xmlns="http://www.w3.org/2000/svg">
						<path d="M512 938.667c-235.264 0-426.667-191.424-426.667-426.645 0-235.264 191.403-426.688 426.667-426.688 235.243 0 426.667 191.424 426.667 426.688 0 235.221-191.424 426.645-426.667 426.645zM512 128c-211.733 0-384 172.267-384 384.021 0 211.733 172.267 383.979 384 383.979s384-172.245 384-383.979c0-211.755-172.267-384.021-384-384.021zM448.448 738.539l-150.379-186.475 33.195-26.795 116.288 144.192 260.843-341.077 33.877 25.899z" />
					</svg>
				</button>
			)}
			<span className={`flex grow text-gray-600 text-sm ${task.isCompleted ? "line-through" : ""}`}>{task.name}</span>
			{hover && (
				<button
					className="text-gray-400 text-2xl"
					onClick={() => dispatch(deleteTask(task.id))}>
					<svg
						className="svg-icon"
						style={{ width: "1em", height: "1em", verticalAlign: "middle", fill: "currentColor", overflow: "hidden" }}
						viewBox="0 0 1024 1024"
						version="1.1"
						xmlns="http://www.w3.org/2000/svg">
						<path d="M557.312 513.248l265.28-263.904c12.544-12.48 12.608-32.704 0.128-45.248-12.512-12.576-32.704-12.608-45.248-0.128l-265.344 263.936-263.04-263.84C236.64 191.584 216.384 191.52 203.84 204 191.328 216.48 191.296 236.736 203.776 249.28l262.976 263.776L201.6 776.8c-12.544 12.48-12.608 32.704-0.128 45.248 6.24 6.272 14.464 9.44 22.688 9.44 8.16 0 16.32-3.104 22.56-9.312l265.216-263.808 265.44 266.24c6.24 6.272 14.432 9.408 22.656 9.408 8.192 0 16.352-3.136 22.592-9.344 12.512-12.48 12.544-32.704 0.064-45.248L557.312 513.248z" />
					</svg>
				</button>
			)}
		</div>
	);
}
