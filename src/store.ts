import { configureStore } from "@reduxjs/toolkit";
import todo from "./features/todo";

export const store = configureStore({
	reducer: {
		todo,
	},
});
