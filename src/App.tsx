import { useState } from "react";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import "./App.css";
import InputTodo from "./components/InputTodo";
import TaskList from "./components/TaskList";

function App() {
	return (
		<div className="bg-slate-800 text-slate-100 h-screen w-screen flex flex-col justify-center items-center">
			<div className="max-w-2xl w-full mx-auto ">
				<div className="flex flex-row justify-between mb-5">
					<p className="text-slate-200 font-bold text-4xl tracking-[0.5em]">TODO</p>
					<span>T</span>
				</div>
				<div className="mb-6">
					<InputTodo />
				</div>
				<div className="mt-6">
					<TaskList />
				</div>
			</div>
		</div>
	);
}

export default App;
